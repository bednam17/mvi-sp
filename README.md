Martin Bednář
# Evaluating language level of argumentative essays

*MVI semestral project*

## Assignment

Full asignment: 
[https://www.kaggle.com/competitions/feedback-prize-english-language-learning/data](https://www.kaggle.com/competitions/feedback-prize-english-language-learning/data)

Briefly, the task is to rate essays written by English language students. There are six measures that should be rated separately: cohesion, syntax, vocabulary, phraseology, grammar, and conventions.

## Data set

The data set can be obtained from the Kaggle competition page or from this repository (`kaggle-data.zip`).

## Launch instructions

Run `kaggle-submission.ipynb`. Make sure that the path to the data set is set properly in the `CFG` class.

## Blog post

I have written a [Medium.com story](https://medium.com/@martin.bednar33/evaluating-language-level-using-pretrained-embeddings-and-pytorch-81c9476ac7c8) about my solution.